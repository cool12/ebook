package com.example.ebook.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ebook.Models.HomeScreenItems;
import com.example.ebook.R;

import java.util.ArrayList;

public class HomeScreenAdapter extends RecyclerView.Adapter<HomeScreenAdapter.HomeScreenAdapterViewHolder> {

    private ArrayList<HomeScreenItems> data = new ArrayList<>();

    public HomeScreenAdapter(ArrayList<HomeScreenItems> loadData) {
        data = loadData;
    }

    @NonNull
    @Override
    public HomeScreenAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_home_screen, viewGroup, false);
        return new HomeScreenAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeScreenAdapterViewHolder homeScreenAdapterViewHolder, int i) {
        String text = data.get(i).getText();
        int image = data.get(i).getImage();

        homeScreenAdapterViewHolder.setImageViewItem(image);
        homeScreenAdapterViewHolder.setTextViewItem(text);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class HomeScreenAdapterViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewItem;
        private TextView textViewItem;

        public HomeScreenAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewItem = itemView.findViewById(R.id.imageViewItem);
            textViewItem = itemView.findViewById(R.id.textViewItem);
        }

        public void setImageViewItem(int image) {
            this.imageViewItem.setImageResource(image);
        }

        public void setTextViewItem(String text) {
            this.textViewItem.setText(text);
        }
    }
}
