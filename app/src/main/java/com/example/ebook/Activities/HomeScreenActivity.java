package com.example.ebook.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.ebook.Adapters.HomeScreenAdapter;
import com.example.ebook.Models.HomeScreenItems;
import com.example.ebook.R;

import java.util.ArrayList;

public class HomeScreenActivity extends AppCompatActivity {
    // Define variables
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager manager;
    private ArrayList<HomeScreenItems> homeScreenItems = new ArrayList<>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Old Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));
        homeScreenItems.add(new HomeScreenItems("Current Law", R.drawable.currentlaw2));

        // Link variables
        mAdapter = new HomeScreenAdapter(homeScreenItems);
        manager = new GridLayoutManager(this, 2);
        mRecyclerView = findViewById(R.id.homeScreenRecyclerView);

        // Recycler view settings
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);
    }
}
